package com.example.saeedspc.logger_androidapp.Fragments;

import android.app.LocalActivityManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.applandeo.materialcalendarview.DatePicker;
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener;
import com.example.saeedspc.logger_androidapp.AggregationResultsTab;
import com.example.saeedspc.logger_androidapp.HttpHandler;
import com.example.saeedspc.logger_androidapp.R;
import com.example.saeedspc.logger_androidapp.SearchResultsTab;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManagementFragment extends Fragment implements View.OnClickListener{

    private String TAG = EventManagementFragment.class.getSimpleName();

    //    private ProgressDialog pDialog;
    private String data_source_url = "http://192.168.101.192/api/v1/plugins";
    private String event_type_url = "http://192.168.101.192/api/v1/plugin-sids";
    private String search_profile_url = "http://192.168.101.192/api/v1/search-profiles";
    private int profileId = 0;

    View view;
    TableLayout mainTableLayout;
    TableRow mainTableRow;
    private String token;
    private Map<Integer,String> profiles;

    Switch log_source_switch;
    Switch payload_switch;
    Switch username_switch;
    Switch data_source_switch;
    Switch event_type_switch;
    Switch protocol_switch;
    Switch all_dates_switch;

    Spinner address_logic_spinner;
    Spinner port_logic_spinner;
    Spinner device_type_spinner;
    Spinner category_spinner;
    Spinner sub_category_spinner;
    Spinner collector_spinner;
    Spinner aggregate_spinner;
    ArrayAdapter<CharSequence> address_logic_adpt;
    ArrayAdapter<CharSequence> port_logic_adpt;
    ArrayAdapter<CharSequence> device_type_adpt;
    ArrayAdapter<CharSequence> category_adpt;
    ArrayAdapter<CharSequence> sub_category_adpt;
    ArrayAdapter<CharSequence> collector_adpt;
    ArrayAdapter<CharSequence> aggregate_adpt;

    LinearLayout parameters, date_specification, profileLayout;

    Button parametersButton, advanceSearch, searchProfile, submit, clear;

    Intent intent, intent2;
    Bundle bundle;
    SearchResultsTab searchResultsTab ;

    EditText source_ip_e ;
    EditText destination_ip_e ;
    EditText source_port_e ;
    EditText destination_port_e ;
    EditText log_source_e ;
    EditText payload_e ;
    EditText username_e ;
    AutoCompleteTextView data_source_e;
    List<String> data_source_suggestions;
    AutoCompleteTextView event_type_e;
    List<String> event_type_suggestions;
    EditText protocol_e ;
    EditText start_date_e ;
    EditText end_date_e ;
    EditText start_time_e ;
    EditText end_time_e ;

    TabHost tabHost ;
    TabHost.TabSpec tab1 ;
    TabHost.TabSpec tab2 ;


    public EventManagementFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Event Management");

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_event_managment, container, false);

        tabHost = view.findViewById(android.R.id.tabhost);
        tab1 = tabHost.newTabSpec("SearchResults");
        tab2 = tabHost.newTabSpec("AggregationResults");
        mainTableLayout = view.findViewById(R.id.saved_profiles_table);
        mainTableRow = view.findViewById(R.id.profiles_first_row);
        profiles = new HashMap<>();

        searchResultsTab = new SearchResultsTab();
        bundle = new Bundle();
        token = getArguments().getString("token");
        data_source_suggestions = new ArrayList<>();
        event_type_suggestions = new ArrayList<>();

        date_specification = view.findViewById(R.id.date_specification);

        log_source_switch = view.findViewById(R.id.log_source_switch);
        log_source_switch.setTextOn("Is");
        log_source_switch.setTextOff("Not");
        log_source_switch.setShowText(true);

        payload_switch = view.findViewById(R.id.payload_switch);
        payload_switch.setTextOn("Is");
        payload_switch.setTextOff("Not");
        payload_switch.setShowText(true);

        username_switch = view.findViewById(R.id.username_switch);
        username_switch.setTextOn("Is");
        username_switch.setTextOff("Not");
        username_switch.setShowText(true);

        data_source_switch = view.findViewById(R.id.data_source_switch);
        data_source_switch.setTextOn("Is");
        data_source_switch.setTextOff("Not");
        data_source_switch.setShowText(true);

        event_type_switch = view.findViewById(R.id.event_type_switch);
        event_type_switch.setTextOn("Is");
        event_type_switch.setTextOff("Not");
        event_type_switch.setShowText(true);

        protocol_switch = view.findViewById(R.id.protocol_switch);
        protocol_switch.setTextOn("Is");
        protocol_switch.setTextOff("Not");
        protocol_switch.setShowText(true);

        all_dates_switch = view.findViewById(R.id.all_date_switch);
        all_dates_switch.setTextOn("All");
        all_dates_switch.setTextOff("Range");
        all_dates_switch.setShowText(true);
        date_specification.setVisibility(View.GONE);
        all_dates_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    date_specification.setVisibility(View.GONE);
                    start_date_e.setText("");
                    end_date_e.setText("");
                    start_time_e.setText("");
                    end_time_e.setText("");
                }
                else
                    date_specification.setVisibility(View.VISIBLE);
            }
        });

        address_logic_spinner = view.findViewById(R.id.address_spinner);
        address_logic_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.operations,R.layout.spinner_item);
        address_logic_adpt.setDropDownViewResource(R.layout.spinner_item);
        address_logic_spinner.setAdapter(address_logic_adpt);

        parameters = view.findViewById(R.id.parameters);
        parameters.setVisibility(View.GONE);
        address_logic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        port_logic_spinner = view.findViewById(R.id.port_spinner);
        port_logic_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.operations,R.layout.spinner_item);
        port_logic_adpt.setDropDownViewResource(R.layout.spinner_item);
        port_logic_spinner.setAdapter(port_logic_adpt);

        port_logic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        parametersButton = view.findViewById(R.id.parameters_button);
        parametersButton.setOnClickListener(this);

        device_type_spinner = view.findViewById(R.id.device_type_spinner);
        device_type_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.device_type,R.layout.spinner_item);
        device_type_adpt.setDropDownViewResource(R.layout.spinner_item);
        device_type_spinner.setAdapter(device_type_adpt);
        device_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        category_spinner = view.findViewById(R.id.category_spinner);
        category_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.category,R.layout.spinner_item);
        category_adpt.setDropDownViewResource(R.layout.spinner_item);
        category_spinner.setAdapter(category_adpt);
        category_spinner.setEnabled(false);
        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sub_category_spinner = view.findViewById(R.id.sub_category_spinner);
        sub_category_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.category,R.layout.spinner_item);
        sub_category_adpt.setDropDownViewResource(R.layout.spinner_item);
        sub_category_spinner.setAdapter(sub_category_adpt);
        sub_category_spinner.setEnabled(false);
        sub_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        collector_spinner = view.findViewById(R.id.collector_spinner);
        collector_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.collector,R.layout.spinner_item);
        collector_adpt.setDropDownViewResource(R.layout.spinner_item);
        collector_spinner.setAdapter(collector_adpt);
        collector_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        aggregate_spinner = view.findViewById(R.id.aggregate_spinner);
        aggregate_adpt = ArrayAdapter.createFromResource(getActivity(),R.array.aggregate_field,R.layout.spinner_item);
        aggregate_adpt.setDropDownViewResource(R.layout.spinner_item);
        aggregate_spinner.setAdapter(aggregate_adpt);
        aggregate_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        advanceSearch = view.findViewById(R.id.advance_search_button);
        advanceSearch.setOnClickListener(new View.OnClickListener() {
            int advanceSearchState = 0;
            @Override
            public void onClick(View view) {

                Drawable icon = getResources().getDrawable(R.drawable.button_back_minus);
                if(advanceSearchState == 0) {
                    advanceSearch.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                    advanceSearchState = 1;
                }
                else if(advanceSearchState == 1) {
                    icon = getResources().getDrawable(R.drawable.button_back_plus);
                    advanceSearch.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                    advanceSearchState = 0;
                }
            }
        });

        profileLayout = view.findViewById(R.id.saved_profiles);
        profileLayout.setVisibility(View.GONE);
        searchProfile = view.findViewById(R.id.search_profile_button);
        searchProfile.setOnClickListener(new View.OnClickListener() {
            int searchProfileState = 0;
            @Override
            public void onClick(View view) {

                Drawable icon = getResources().getDrawable(R.drawable.button_back_minus);
                if(profileLayout.getVisibility() == View.GONE) {
                    searchProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                    mainTableLayout.removeAllViewsInLayout();

                    TableRow tableRow = new TableRow(getContext());
                    tableRow.setLayoutParams(mainTableRow.getLayoutParams());

                    TextView textView1 = new TextView(getContext());
                    textView1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    textView1.setBackground(getResources().getDrawable(R.drawable.cell_border));
                    textView1.setTextColor(Color.parseColor("#000000"));
                    textView1.setText("Profile Name");
                    textView1.setPadding(dpToPx(10),dpToPx(10),dpToPx(10),dpToPx(10));
                    tableRow.addView(textView1);
                    TextView textView2 = new TextView(getContext());
                    textView2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    textView2.setBackground(getResources().getDrawable(R.drawable.cell_border));
                    textView2.setTextColor(Color.parseColor("#000000"));
                    textView2.setText("Actions");
                    textView2.setPadding(dpToPx(10),dpToPx(10),dpToPx(10),dpToPx(10));
                    tableRow.addView(textView2);
                    mainTableLayout.addView(tableRow);
                    profileLayout.setVisibility(View.VISIBLE);
                    SavedProfiles savedProfiles = new SavedProfiles();
                    savedProfiles.execute();
                }
                else if(profileLayout.getVisibility() == View.VISIBLE) {
                    icon = getResources().getDrawable(R.drawable.button_back_plus);
                    searchProfile.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                    profileLayout.setVisibility(View.GONE);
                }
            }
        });

        LocalActivityManager mlam = new LocalActivityManager(getActivity(), false);
        mlam.dispatchCreate(savedInstanceState);
        tabHost.setup(mlam);

        tab1.setIndicator("Search Results");
        tab2.setIndicator("Aggregation Results");

        source_ip_e = view.findViewById(R.id.source_ip);
        destination_ip_e =  view.findViewById(R.id.destination_ip);
        source_port_e = view.findViewById(R.id.source_port);
        destination_port_e = view.findViewById(R.id.destination_port);
        log_source_e = view.findViewById(R.id.log_source);
        payload_e = view.findViewById(R.id.payload);
        username_e = view.findViewById(R.id.username);
        data_source_e = view.findViewById(R.id.data_source);
        data_source_e.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SimpleSearch simpleSearch = new SimpleSearch();
                simpleSearch.execute();
                if(data_source_suggestions.contains(data_source_e.getText().toString())) {
                    data_source_e.clearFocus();
                    data_source_e.dismissDropDown();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        event_type_e = view.findViewById(R.id.event_type);
        event_type_e.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SimpleSearch simpleSearch = new SimpleSearch();
                simpleSearch.execute();
                if(event_type_suggestions.contains(event_type_e.getText().toString()))
                    event_type_e.clearFocus();
                event_type_e.dismissDropDown();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        protocol_e = view.findViewById(R.id.protocol);
        start_date_e = view.findViewById(R.id.start_date);
        start_date_e.setOnClickListener(this);
        start_date_e.setKeyListener(null);
        start_date_e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                    Date date = new Date();
                    try {
                        date = format.parse(start_date_e.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    calendar.setTime(date);
                    DatePicker.Builder builder = new DatePicker.Builder(getActivity(), listener).date(calendar);
                    DatePicker datePicker = builder.build();
                    datePicker.show();
                }
            }
        });
        end_date_e = view.findViewById(R.id.end_date);
        end_date_e.setOnClickListener(this);
        end_date_e.setKeyListener(null);
        end_date_e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                    Date date = new Date();
                    try {
                        date = format.parse(end_date_e.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    calendar.setTime(date);
                    DatePicker.Builder builder = new DatePicker.Builder(getActivity(), listener).date(calendar);
                    DatePicker datePicker = builder.build();
                    datePicker.show();
                }
            }
        });
        start_time_e = view.findViewById(R.id.start_time);
        start_time_e.setOnClickListener(this);
        start_time_e.setKeyListener(null);
        start_time_e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
                    Date date = new Date();
                    try {
                        date = format.parse(start_time_e.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    calendar.setTime(date);
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT,  new TimePickerDialog.OnTimeSetListener() {
                        String format;
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            if (selectedHour == 0) {
                                format = "AM";
                            }
                            else if (selectedHour == 12) {
                                format = "PM";
                            }
                            else if (selectedHour > 12) {
                                format = "PM";
                            }
                            else {
                                format = "AM";
                            }
                            start_time_e.setText(selectedHour + ":" + selectedMinute + ":00 " + format);
                        }
                    }, hour, minute, false);
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            }
        });
        end_time_e = view.findViewById(R.id.end_time);
        end_time_e.setOnClickListener(this);
        end_time_e.setKeyListener(null);
        end_time_e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
                    Date date = new Date();
                    try {
                        date = format.parse(end_time_e.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    calendar.setTime(date);
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT,  new TimePickerDialog.OnTimeSetListener() {
                        String format;
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            if (selectedHour == 0) {
                                format = "AM";
                            }
                            else if (selectedHour == 12) {
                                format = "PM";
                            }
                            else if (selectedHour > 12) {
                                format = "PM";
                            }
                            else {
                                format = "AM";
                            }
                            end_time_e.setText(selectedHour + ":" + selectedMinute + ":00 " + format);
                        }
                    }, hour, minute, false);
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            }
        });

        String source_ip = "&source-address=" + source_ip_e.getText().toString();
        String address_logic = "&address-logic=" + address_logic_spinner.getSelectedItemId();
        String destination_ip = "&destination-address=" + destination_ip_e.getText().toString();
        String source_port = "&source-port=" + source_port_e.getText().toString();
        String port_logic = "&port-logic=" + port_logic_spinner.getSelectedItemId();
        String destination_port = "&destination-port=" + destination_port_e.getText().toString();
        String log_source = "&log-source=" + log_source_e.getText().toString() + "&log-source-bool=" + log_source_switch.isChecked();
        String payload = "&payload=" + payload_e.getText().toString() + "&payload-bool=" + payload_switch.isChecked();
        String username = "&username=" + username_e.getText().toString() + "&username-bool=" + username_switch.isChecked();
        String data_source = "&data-source=" + data_source_e.getText().toString() + "&data-source-bool=" + data_source_switch.isChecked();
        String event_type = "&event-type=" + event_type_e.getText().toString() + "&event-type-bool=" + event_type_switch.isChecked();
        String protocol = "&protocol=" + protocol_e.getText().toString() + "&protocol-bool=" + protocol_switch.isChecked();
        String all_dates = "&all-dates=" + all_dates_switch.isChecked();
        String date = "&start-date=" + start_date_e.getText() + " " + start_time_e.getText()
                + "&end-date=" + end_date_e.getText() + " " + end_time_e.getText();
        String date_range = date.replace(" ", "%20").replace("/", "%2F").replace(":","%3A");
        String aggregate = "&aggregate-field=" + valueOfAgg(aggregate_spinner.getSelectedItemId());

        intent = new Intent(getActivity(),searchResultsTab.getClass());

        bundle.putString("source_ip",source_ip);
        bundle.putString("address_logic",address_logic);
        bundle.putString("destination_ip", destination_ip);
        bundle.putString("source_port", source_port);
        bundle.putString("port_logic",port_logic);
        bundle.putString("destination_port", destination_port);
        bundle.putString("log_source", log_source);
        bundle.putString("payload",payload);
        bundle.putString("username", username);
        bundle.putString("data_source",data_source);
        bundle.putString("event_type",event_type);
        bundle.putString("protocol", protocol);
        bundle.putString("all_dates",all_dates);
        bundle.putString("date_range",date_range);
        bundle.putString("aggregate_field",aggregate);
        bundle.putString("token",token);

        intent.putExtra("bundle", bundle);

        tab1.setContent(intent);

        intent2 = new Intent(getActivity(),AggregationResultsTab.class);
        intent2.putExtra("bundle", bundle);
        tab2.setContent(intent2);

        tabHost.addTab(tab1);
        tabHost.addTab(tab2);

        final ScrollView scrollView = view.findViewById(R.id.scroll);
        scrollView.smoothScrollTo(0,0);


        submit = view.findViewById(R.id.submit_button);
        submit.setOnClickListener(this);
        clear = view.findViewById(R.id.clear_button);
        clear.setOnClickListener(this);

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                scrollView.smoothScrollTo(scrollView.getScrollX(),scrollView.getScrollY());
            }
        });

        return view;
    }

    private String valueOfAgg(long i) {
        String value;
        int j = (int) i;
        switch(j){
            case 1:
                value = "SIP.raw";
                break;
            case 2:
                value = "DIP.raw";
                break;
            case 3:
                value = "PID.raw,PSID.raw";
                break;
            case 4:
                value = "SPort.raw";
                break;
            case 5:
                value = "DPort.raw";
                break;
            case 6:
                value = "Username.raw";
                break;
            case 7:
                value = "Log_source.raw";
                break;
            case 8:
                value = "Protocol.raw";
                break;
            case 9:
                value = "Collector.raw";
                break;
            default:
                value = "SIP.raw";
                break;
        }
        return value;

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.submit_button){
            tabHost.clearAllTabs();
            intent = new Intent(getActivity(),SearchResultsTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent2 = new Intent(getActivity(),AggregationResultsTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            String source_ip = "&source-address=" + source_ip_e.getText().toString();
            String address_logic = "&address-logic=" + address_logic_spinner.getSelectedItemId();
            String destination_ip = "&destination-address=" + destination_ip_e.getText().toString();
            String source_port = "&source-port=" + source_port_e.getText().toString();
            String port_logic = "&port-logic=" + port_logic_spinner.getSelectedItemId();
            String destination_port = "&destination-port=" + destination_port_e.getText().toString();
            String log_source = "&log-source=" + log_source_e.getText().toString() + "&log-source-bool=" + log_source_switch.isChecked();
            String payload = "&payload=" + payload_e.getText().toString() + "&payload-bool=" + payload_switch.isChecked();
            String username = "&username=" + username_e.getText().toString() + "&username-bool=" + username_switch.isChecked();
            String data_source = "&data-source=" + data_source_e.getText().toString() + "&data-source-bool=" + data_source_switch.isChecked();
            String event_type = "&event-type=" + event_type_e.getText().toString() + "&event-type-bool=" + event_type_switch.isChecked();
            String protocol = "&protocol=" + protocol_e.getText().toString() + "&protocol-bool=" + protocol_switch.isChecked();
            String all_dates = "&all-dates=" + all_dates_switch.isChecked();
            String date = "&start-date=" + start_date_e.getText() + " " + start_time_e.getText()
                    + "&end-date=" + end_date_e.getText() + " " + end_time_e.getText();
            String date_range = date.replace(" ", "%20").replace("/", "%2F").replace(":","%3A");
            String aggregate = "&aggregate-field=" + valueOfAgg(aggregate_spinner.getSelectedItemId());

            bundle.putString("source_ip",source_ip);
            bundle.putString("address_logic",address_logic);
            bundle.putString("destination_ip", destination_ip);
            bundle.putString("source_port", source_port);
            bundle.putString("port_logic",port_logic);
            bundle.putString("destination_port", destination_port);
            bundle.putString("log_source", log_source);
            bundle.putString("payload",payload);
            bundle.putString("username", username);
            bundle.putString("data_source",data_source);
            bundle.putString("event_type",event_type);
            bundle.putString("protocol", protocol);
            bundle.putString("all_dates",all_dates);
            bundle.putString("date_range",date_range);
            bundle.putString("aggregate_field",aggregate);
            bundle.putString("token",token);

            intent.putExtra("bundle", bundle);
            tab1 = tabHost.newTabSpec("SearchResults")
                    .setIndicator("Search Results")
                    .setContent(intent);
            intent2.putExtra("bundle", bundle);
            tab2 = tabHost.newTabSpec("AggregationResults")
                    .setIndicator("Aggregation Results")
                    .setContent(intent2);
            tabHost.addTab(tab1);
            tabHost.addTab(tab2);
        } else if(view.getId() == R.id.clear_button) {
            source_ip_e.setText("");
            address_logic_spinner.setSelection(0);
            destination_ip_e.setText("");
            source_port_e.setText("");
            port_logic_spinner.setSelection(0);
            destination_port_e.setText("");
            log_source_e.setText("");
            log_source_switch.setChecked(true);
            payload_e.setText("");
            payload_switch.setChecked(true);
            username_e.setText("");
            username_switch.setChecked(true);
            data_source_e.setText("");
            data_source_switch.setChecked(true);
            event_type_e.setText("");
            event_type_switch.setChecked(true);
            protocol_e.setText("");
            protocol_switch.setChecked(true);
            device_type_spinner.setSelection(0);
            category_spinner.setSelection(0);
            sub_category_spinner.setSelection(0);
            collector_spinner.setSelection(0);
            all_dates_switch.setChecked(true);
            start_date_e.setText("");
            end_date_e.setText("");
            start_time_e.setText("");
            end_time_e.setText("");
        } else if(view.getId() == R.id.start_date){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date date = new Date();
            try {
                date = format.parse(start_date_e.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            DatePicker.Builder builder = new DatePicker.Builder(getActivity(), listener).date(calendar);
            DatePicker datePicker = builder.build();
            datePicker.show();
        } else if(view.getId() == R.id.end_date){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            Date date = new Date();
            try {
                date = format.parse(end_date_e.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            DatePicker.Builder builder = new DatePicker.Builder(getActivity(), listener).date(calendar);
            DatePicker datePicker = builder.build();
            datePicker.show();
        } else if(view.getId() == R.id.start_time){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
            Date date = new Date();
            try {
                date = format.parse(start_time_e.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT,  new TimePickerDialog.OnTimeSetListener() {
                String format;
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        format = "AM";
                    }
                    else if (selectedHour == 12) {
                        format = "PM";
                    }
                    else if (selectedHour > 12) {
                        format = "PM";
                    }
                    else {
                        format = "AM";
                    }
                    start_time_e.setText(selectedHour + ":" + selectedMinute + ":00 " + format);
                }
            }, hour, minute, false);
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        } else if(view.getId() == R.id.end_time){
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
            Date date = new Date();
            try {
                date = format.parse(end_time_e.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            calendar.setTime(date);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(),TimePickerDialog.THEME_HOLO_LIGHT,  new TimePickerDialog.OnTimeSetListener() {
                String format;
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    if (selectedHour == 0) {
                        format = "AM";
                    }
                    else if (selectedHour == 12) {
                        format = "PM";
                    }
                    else if (selectedHour > 12) {
                        format = "PM";
                    }
                    else {
                        format = "AM";
                    }


                    end_time_e.setText(selectedHour + ":" + selectedMinute + ":00 " + format);
                }
            }, hour, minute, false);
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        } else if(view.getId() == R.id.parameters_button){

            if(parameters.getVisibility() == View.GONE) {
                Drawable icon = getResources().getDrawable(R.drawable.button_back_minus);
                parametersButton.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                parameters.setVisibility(View.VISIBLE);
            }
            else if(parameters.getVisibility() == View.VISIBLE) {
                Drawable icon = getResources().getDrawable(R.drawable.button_back_plus);
                parametersButton.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                parameters.setVisibility(View.GONE);
            }
        } else if(view.getId() == R.id.search_profile_button){

        }
    }

    private OnSelectDateListener listener = new OnSelectDateListener() {
        @Override
        public void onSelect(Calendar calendar) {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");

            if(start_date_e.isFocused())
                start_date_e.setText(simpleDateFormat.format(calendar.getTime()));
            else
                end_date_e.setText(simpleDateFormat.format(calendar.getTime()));
        }
    };

    private class SimpleSearch extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String jsonStr;
            HttpHandler hh = new HttpHandler();

            String ds = "&query=" + data_source_e.getText();
            String ett = "&query=" + event_type_e.getText();
            String et = ett.replace(" ", "%20");


            if(data_source_e.isFocused())
                jsonStr = hh.makeServiceCall(data_source_url + "?token=" + token + ds, "GET");
            else
                jsonStr = hh.makeServiceCall(event_type_url + "?token=" + token + et, "GET");

            Log.e(TAG, "Response from url: " + jsonStr);

            data_source_suggestions.clear();
            event_type_suggestions.clear();

            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);

                    JSONArray results = jsonObject.getJSONArray("data");

                    for (int i = 0; i < results.length(); i++) {
                        JSONObject jo = results.getJSONObject(i);
                        String name = jo.getString("name");
                        if(data_source_e.isFocused())
                            data_source_suggestions.add(name);
                        else
                            event_type_suggestions.add(name);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "JSON parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Could'nt get json from server!:(");
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (data_source_e.isFocused()) {
                ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_item,data_source_suggestions);
                data_source_e.setAdapter(adapter);
                data_source_e.showDropDown();
            } else if(event_type_e.isFocused()){
                ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_item,event_type_suggestions);
                event_type_e.setAdapter(adapter);
                event_type_e.showDropDown();
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
    private class SavedProfiles extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String jsonStr;
            HttpHandler hh = new HttpHandler();

            jsonStr = hh.makeServiceCall(search_profile_url + "?token=" + token , "GET");

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);

                    JSONArray results = jsonObject.getJSONArray("data");

                    for (int i = 0; i < results.length(); i++) {
                        JSONObject jo = results.getJSONObject(i);
                        String name = jo.getString("name");
                        int id = jo.getInt("id");
                        profiles.put(id,name);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "JSON parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Could'nt get json from server!:(");
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (Map.Entry<Integer, String> entry : profiles.entrySet()) {
                int id = entry.getKey();
                String name = entry.getValue();

                TableRow tableRow = createTableRow(name,id);
                mainTableLayout.addView(tableRow);
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    private class LoadProfile extends AsyncTask<Void, Void, Void> {

        private String source_address_profile;
        private int address_logic_profile;
        private String destination_address_profile;
        private String source_port_profile;
        private int port_logic_profile;
        private String destination_port_profile;
        private String log_source_profile;
        private boolean log_source_switch_profile;
        private String payload_profile;
        private boolean payload_switch_profile;
        private String username_profile;
        private boolean username_switch_profile;
        private String data_source_profile;
        private boolean data_source_switch_profile;
        private String event_type_profile;
        private boolean event_type_switch_profile;
        private String protocol_profile;
        private boolean protocol_switch_profile;
        private int device_type_profile;
        private int category_profile;
        private int sub_category_profile;
        private int collector_profile;
        private boolean all_dates_switch_profile;

        @Override
        protected Void doInBackground(Void... voids) {
            String jsonStr;
            HttpHandler hh = new HttpHandler();

            jsonStr = hh.makeServiceCall(search_profile_url + "/" + profileId + "?token=" + token , "GET");

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);

                    JSONObject data = jsonObject.getJSONObject("data");

                    String fields = data.getString("fields");
                    data = new JSONObject(fields);
                    source_address_profile = data.getString("source-address");
                    address_logic_profile = data.getInt("address-logic");
                    destination_address_profile = data.getString("destination-address");
                    source_port_profile = data.getString("source-port");
                    port_logic_profile = data.getInt("port-logic");
                    destination_port_profile = data.getString("destination-port");
                    log_source_profile = data.getString("log-source");
                    log_source_switch_profile = data.getBoolean("log-source-bool");
                    payload_profile = data.getString("payload");
                    payload_switch_profile = data.getBoolean("payload-bool");
                    username_profile = data.getString("username");
                    username_switch_profile = data.getBoolean("username-bool");
                    data_source_profile = data.getString("data-source");
                    data_source_switch_profile = data.getBoolean("data-source-bool");
                    event_type_profile = data.getString("event-type");
                    event_type_switch_profile = data.getBoolean("event-type-bool");
                    protocol_profile = data.getString("protocol");
                    protocol_switch_profile = data.getBoolean("protocol-bool");
//                    device_type_profile = data.getInt("device-type");
//                    category_profile = data.getInt("category");
//                    sub_category_profile = data.getInt("sub-category");
//                    collector_profile = data.getInt("collector");
                    all_dates_switch_profile = data.getBoolean("all-dates");
                } catch (final JSONException e) {
                    Log.e(TAG, "JSON parsing error: " + e.getMessage());
                }
            } else {
                Log.e(TAG, "Could'nt get json from server!:(");
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            source_ip_e.setText(source_address_profile);
            address_logic_spinner.setSelection(address_logic_profile);
            destination_ip_e.setText(destination_address_profile);
            source_port_e.setText(source_port_profile);
            port_logic_spinner.setSelection(port_logic_profile);
            destination_port_e.setText(destination_port_profile);
            log_source_e.setText(log_source_profile);
            log_source_switch.setChecked(log_source_switch_profile);
            payload_e.setText(payload_profile);
            payload_switch.setChecked(payload_switch_profile);
            username_e.setText(username_profile);
            username_switch.setChecked(username_switch_profile);
            data_source_e.setText(data_source_profile);
            data_source_switch.setChecked(data_source_switch_profile);
            event_type_e.setText(event_type_profile);
            event_type_switch.setChecked(event_type_switch_profile);
            protocol_e.setText(protocol_profile);
            protocol_switch.setChecked(protocol_switch_profile);
//            device_type_spinner.setSelection(device_type_profile);
//            category_spinner.setSelection(category_profile);
//            sub_category_spinner.setSelection(sub_category_profile);
//            collector_spinner.setSelection(collector_profile);
            all_dates_switch.setChecked(all_dates_switch_profile);
        }
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    private TableRow createTableRow(String s, final int id){
        TableRow tableRow = new TableRow(getContext());
        tableRow.setLayoutParams(mainTableRow.getLayoutParams());

        Button myButton;
        myButton = new Button(getContext());
        myButton.setText("Load Profile");
        myButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
//        final int sdk = Build.VERSION.SDK_INT;
//        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
//            myButton.setBackgroundDrawable(getResources().getDrawable(custom_enabled_button));
//        }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            myButton.setBackground(getResources().getDrawable(custom_enabled_button));
//        }
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileId = id;
                LoadProfile loadProfile = new LoadProfile();
                loadProfile.execute();
            }
        });

        TextView textView = new TextView(getContext());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setText(s);
        textView.setPadding(dpToPx(10),0,0,0);
        tableRow.addView(textView);
        tableRow.addView(myButton);
        return tableRow;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

}
