package com.example.saeedspc.logger_androidapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

import static com.example.saeedspc.logger_androidapp.R.drawable.cell_border;


@SuppressWarnings({"FieldCanBeLocal", "deprecation"})
public class AggregationResultsTab extends AppCompatActivity {

    private String TAG = AggregationResultsTab.class.getSimpleName();

    private int totalNumItems;
    private int itemsPerPage;
    private int totalPages;
    private int currentPage;

    Vector<TableRow> myRows;
    private String source_ip;
    private String address_logic;
    private String destination_ip;
    private String source_port;
    private String port_logic;
    private String destination_port;
    private String log_source;
    private String payload;
    private String username;
    private String data_source;
    private String event_type;
    private String protocol;
    private String all_dates;
    private String date_range;
    private String aggregate_field;

    private String token;
    SimpleSearch simpleSearch;
    private ProgressDialog pDialog;
    private static String url = "http://192.168.101.192/api/v1/security-events/aggregation";

    Spinner spinner;
    ArrayAdapter<CharSequence> adapter;
    TableLayout mainTableLayout;
    TableRow mainTableRow;
    Bundle bundle;
    Intent starterIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aggregation_results_tab);

        bundle = getIntent().getBundleExtra("bundle");
        starterIntent = getIntent();

        myRows = new Vector<>();
        currentPage = 1;

        spinner = (Spinner)findViewById(R.id.aggregation_spinner);
        adapter = ArrayAdapter.createFromResource(this,R.array.per_page,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = spinner.getSelectedItem().toString();
                itemsPerPage = Integer.parseInt(text);
                currentPage = 1;
                attemptSearch();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mainTableLayout = (TableLayout) findViewById(R.id.aggregation_table);
        mainTableRow = (TableRow) findViewById(R.id.aggregation_first_row);

//        attemptSearch();

//        generatePage(currentPage);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void attemptSearch() {

        source_ip = bundle.getString("source_ip");
        address_logic = bundle.getString("address_logic");
        destination_ip = bundle.getString("destination_ip");
        source_port = bundle.getString("source_port");
        port_logic = bundle.getString("port_logic");
        destination_port = bundle.getString("destination_port");
        log_source = bundle.getString("log_source");
        payload = bundle.getString("payload");
        username = bundle.getString("username");
        data_source = bundle.getString("data_source");
        event_type = bundle.getString("event_type");
        protocol = bundle.getString("protocol");
        all_dates = bundle.getString("all_dates");
        date_range = bundle.getString("date_range");
        aggregate_field = bundle.getString("aggregate_field");
        token = bundle.getString("token");
        simpleSearch = new SimpleSearch();
        mainTableLayout.removeAllViewsInLayout();
        myRows.removeAllElements();
        TableRow tableRow;
        tableRow = createRow("Aggregate", "Occurrences");
        addToTable(tableRow);
        simpleSearch.execute();
        simpleSearch = null;
    }

    private class SimpleSearch extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String jsonStr;
            HttpHandler hh = new HttpHandler();
            jsonStr = hh.makeServiceCall(url + "?token=" + token + source_ip + address_logic
                    + destination_ip + source_port + port_logic + destination_port + log_source + payload
                    + username + data_source + event_type + protocol + all_dates + date_range + "&limit="
                    + itemsPerPage + "&page=" + currentPage + aggregate_field + "&sort=-@timestamp","GET");

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);

                    JSONArray results = jsonObject.getJSONArray("data");

                    for (int i = 0; i < results.length(); i++) {
                        JSONArray ja = results.getJSONArray(i);
                        int occurrences = ja.getInt(0);
                        String aggregate = ja.getString(1);
                        myRows.add(createRow(aggregate,String.valueOf(occurrences)));
                        JSONObject pagination = jsonObject.getJSONObject("pagination");
                        totalNumItems = Integer.parseInt(pagination.getString("total_count"));
                        totalPages = Integer.parseInt(pagination.getString("total_pages"));
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "JSON parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AggregationResultsTab.this,
                                    "JSON parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                Log.e(TAG, "Could'nt get json from server!:(");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AggregationResultsTab.this,
                                "Could'nt get json from server!:(",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            for(TableRow t: myRows){
                mainTableLayout.addView(t);
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(AggregationResultsTab.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

    }

    public boolean addToTable(TableRow tr){

        if(tr == null) {
            return false;
        }

        mainTableLayout.addView(tr);
        return true;
    }

    public TableRow createRow(String aggregate, String occurrences) {

        TableRow tableRow = new TableRow(this);
        tableRow.setLayoutParams(mainTableRow.getLayoutParams());

        TextView textView7 = buildTextView(aggregate);
        TextView textView8 = buildTextView(occurrences);

        tableRow.addView(textView7);
        tableRow.addView(textView8);

        return tableRow;
    }

    private TextView buildTextView(String s){

        TextView textView = new TextView(this);

        setPadding(textView);

        setBorder(textView);

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

        textView.setText(s);

        return textView;
    }

    private void setBorder(TextView textView){
        final int sdk = Build.VERSION.SDK_INT;
        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
            textView.setBackgroundDrawable(getResources().getDrawable(cell_border));
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                textView.setBackground(getResources().getDrawable(cell_border));
            }
        }
    }

    private void setPadding(TextView textView){
        int left = dpToPx(10);
        int top = dpToPx(10);
        int right = dpToPx(10);
        int bottom = dpToPx(10);

        textView.setPadding(left, top, right, bottom);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.onCreate(intent.getBundleExtra("bundle"));
    }

}